# crypto-gmsm

#### 介绍
国密商密算法（SM2,SM3,SM4）工具类封装

国产密码算法（国密算法）是指国家密码局认定的国产商用密码算法，目前主要使用公开的SM2、SM3、SM4三类算法，分别是非对称算法、哈希算法和对称算法。


1. SM2算法：SM2椭圆曲线公钥密码算法是我国自主设计的公钥密码算法，包括SM2-1椭圆曲线数字签名算法，SM2-2椭圆曲线密钥交换协议，SM2-3椭圆曲线公钥加密算法，分别用于实现数字签名密钥协商和数据加密等功能。SM2算法与RSA算法不同的是，SM2算法是基于椭圆曲线上点群离散对数难题，相对于RSA算法，256位的SM2密码强度已经比2048位的RSA密码强度要高。椭圆曲线参数并没有给出推荐的曲线，曲线参数的产生需要利用一定的算法产生。但在实际使用中，国密局推荐使用素数域256 位椭圆曲线，其曲线方程为y^2= x^3+ax+b（其中p是大于3的一个大素数，n是基点G的阶，Gx、Gy 分别是基点G的x与y值，a、b是随圆曲线方程y^2= x^3+ax+b的系数）。


2. SM3算法：SM3杂凑算法是我国自主设计的密码杂凑算法，适用于商用密码应用中的数字签名和验证消息认证码的生成与验证以及随机数的生成，可满足多种密码应用的安全需求。为了保证杂凑算法的安全性，其产生的杂凑值的长度不应太短，例如MD5输出128比特杂凑值，输出长度太短，影响其安全性SHA-1算法的输出长度为160比特，SM3算法的输出长度为256比特，因此SM3算法的安全性要高于MD5算法和SHA-1算法。


3. SM4算法：SM4分组密码算法是我国自主设计的分组对称密码算法，用于实现数据的加密/解密运算，以保证数据和信息的机密性。要保证一个对称密码算法的安全性的基本条件是其具备足够的密钥长度，SM4算法与AES算法具有相同的密钥长度分组长度128比特，因此在安全性上高于3DES算法。

上述引用自：https://zhuanlan.zhihu.com/p/89791842

#### 软件架构
软件架构说明


#### 安装教程

1.  JDK 8+
2.  mvn clean package

#### 使用说明
0.  导入Maven依赖
```
        <dependency>
            <groupId>cn.superfw</groupId>
            <artifactId>crypto-gmsm</artifactId>
            <version>1.0</version>
        </dependency>
```

1.  SM2使用
```
        String str = "国密商密";

        // 以非压缩公钥模式生成SM2秘钥对（通常）, 对方是其他语言（比如GO语言）实现时也能支持
        // 压缩模式仅限于数据交互的双方都使用BC库的情况
        SM2KeyPair sm2Keys = SM2.generateSm2Keys(false);

        String pubKey = sm2Keys.getPublicKey();
        String prvKey = sm2Keys.getPrivateKey();

        System.out.println("Private Key: " + prvKey);
        System.out.println("Public Key: " + pubKey);

        System.out.println("\n加密解密测试");
        // 加解密测试(C1C2C3模式)
        System.out.println("----- C1C2C3模式 -----");
        try {
            System.out.println("加密前：" + str);
            String enData = SM2.encrypt(pubKey, str, SM2EngineExtend.CIPHER_MODE_BC);
            System.out.println("加密后：" + enData);
            String deData = SM2.decrypt(prvKey, enData, SM2EngineExtend.CIPHER_MODE_BC);
            System.out.println("解密后：" + deData);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("加解密测试错误");
        }

        // 加解密测试(C1C3C2模式)
        System.out.println("----- C1C3C2模式 -----");
        try {
            System.out.println("加密前：" + str);
            String enData = SM2.encrypt(pubKey, str, SM2EngineExtend.CIPHER_MODE_NORM);
            System.out.println("加密后：" + enData);
            String deData = SM2.decrypt(prvKey, enData, SM2EngineExtend.CIPHER_MODE_NORM);
            System.out.println("解密后：" + deData);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("加解密测试错误");
        }

        System.out.println("\n签名验签测试");
        // 签名和验签测试
        try {
            System.out.println("数据：" + str);
            String signStr = SM2.sign(prvKey, str);
            System.out.println("签名：" + signStr);
            boolean verify = SM2.verify(pubKey, str, signStr);
            System.out.println("验签结果：" + verify);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("签名和验签测试错误");
        }
```
2.  SM3使用
```
        SM3.digest("国密商密")
```
3.  SM4使用
```
        String key = SM4.generateKey();
        String input = "国密商密";

        System.out.println("Key：" + key);
        System.out.println("原文：" + input);

        System.out.println();

        System.out.println("ECB模式");

        String output = SM4.encrypt(key, input);
        System.out.println("加密：" + output);
        System.out.println("解密：" + SM4.decrypt(key, output));

        System.out.println();

        System.out.println("CBC模式");
        String iv = "12345678123456781234567812345678";
        output = SM4.encrypt(key, input, iv);
        System.out.println("加密：" + output);
        System.out.println("解密：" + SM4.decrypt(key, output, iv));
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

