package cn.superfw.crypto.gmsm;

import cn.superfw.crypto.gmsm.sm4.SM4;
import org.junit.jupiter.api.Test;

public class SM4Test {

    @Test
    public void testSm4() {
        String key = SM4.generateKey();
        String input = "国密商密";

        System.out.println("Key：" + key);
        System.out.println("原文：" + input);

        System.out.println();

        System.out.println("ECB模式");

        String output = SM4.encrypt(key, input);
        System.out.println("加密：" + output);
        System.out.println("解密：" + SM4.decrypt(key, output));

        System.out.println();

        System.out.println("CBC模式");
        String iv = "12345678123456781234567812345678";
        output = SM4.encrypt(key, input, iv);
        System.out.println("加密：" + output);
        System.out.println("解密：" + SM4.decrypt(key, output, iv));
    }
}
