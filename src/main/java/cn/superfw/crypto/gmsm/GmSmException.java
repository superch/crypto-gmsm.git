package cn.superfw.crypto.gmsm;

public class GmSmException extends RuntimeException {
    public GmSmException() {
    }

    public GmSmException(String message) {
        super(message);
    }

    public GmSmException(String message, Throwable cause) {
        super(message, cause);
    }

    public GmSmException(Throwable cause) {
        super(cause);
    }

    public GmSmException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
